#!/usr/bin/python
import os
import sys
from pyparsing import cppStyleComment
from pyparsing import quotedString
import xlwt
import getopt

"""
This script has been created as a gift to Matus Plachy by Dominik Friml. Its purpose is purely to ease CLR analysis. No further support, development or tweaks by Dominik Friml are to be expected.
"""

__author__ = "Dominik Friml"
__copyright__ = "Copyright 2020"
__credits__ = ["Matus Plachy"]
__license__ = "GPLv3"
__version__ = "1.0.0"
__maintainer__ = "Dominik Friml"
__status__ = "Production"

### Global variables
usageString = """
usage: python3 autoCLR.py [option] path 

\tThis script finds recursively all *.c and *.h files in specified path to find comments placed above all code. It is usable to simplify CLR check.

Options and arguments:
\t-h|--help       \t: print this message
\t-d|--depth num  \t: script analyses first num comments before code. Default: 99
\t-o|--output name\t: specify output file name. Default: output.xls
\t-v|--verbose    \t: if this flag is specified, human readable CLR protocol is printed to console

usage examples:
\tpython autoCLR.py -v -o protocolfile .
\tpython3 autoCLR.py -d 1 --output fooBarRepoCLR.xls ./foo/bar/Repo
\tpython autoCLR.py .

dependencies:
\t- python3
\t- pyparsing
\t- xlwt
\t- getopt
    """
comment = cppStyleComment[0,99]
walk_dir = sys.argv[-1]
listOfHeaders = []
style0 = xlwt.easyxf("font: name Arial; align: horiz right")
style1 = xlwt.easyxf("font: name Arial, bold on; align: horiz left")
style2 = xlwt.easyxf("font: name Consolas; align: horiz left, wrap True")
style3 = xlwt.easyxf("font: name Consolas, bold on; align: horiz centre, vert centre, wrap True")
wb = xlwt.Workbook()
linecounter = 1;
### Prepare worksheet in output protocol
ws1 = wb.add_sheet('Files overview')
ws1.col(0).width = 20000;
ws1.write(0,0,"Path to file",style3)
ws1.write(0,1,"Header found",style3)

### Help message
def usage():
    print(usageString)

### Parsing header comments of file
def parseHeader(file_path, verbose):
    global linecounter
    with open(file_path, 'r') as f: # Open file
        f_content = f.read()
        cmt = next(comment.ignore(quotedString).scanString(f_content)) # Extract comments
        for i in cmt[0]: # Find if comment already present. If not, add it as a new one
            try:
                index = listOfHeaders.index(i) # Is comment already in listOfHeaders?
                if verbose:
                    print(file_path + "\t\t\t\t\t\tcontains header number " + str(listOfHeaders.index(i)))
                ws1.write(linecounter,0,file_path,style0)
                ws1.write(linecounter,1,listOfHeaders.index(i),style1)
            except ValueError:
                listOfHeaders.append(i) # Append new comment to listOfHeaders
                if verbose:
                    print(file_path + "\t\t\t\t\t\tcontains header number " + str(listOfHeaders.index(i)))
                ws1.write(linecounter,0,file_path,style0)
                ws1.write(linecounter,1,listOfHeaders.index(i),style1)
            linecounter += 1

### Main function of script
def main():
    ### Parsing arguments
    try:
        opts, args = getopt.getopt(sys.argv[1:], "ho:vd:", ["help", "output=", "verbose", "depth="])
    except getopt.GetoptError as err:
        print(str(err))  # will print something like "option -a not recognized"
        usage()
        sys.exit(2)
    output = 'output.xls'
    verbose = False
    for o, a in opts:
        if o == "-v":
            verbose = True
        elif o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("-o", "--output"):
            if a.endswith(".xls"):
                output = a
            else:
                output = a + ".xls"
        elif o in ("-d", "--depth"):
            global comment
            comment = cppStyleComment[0,int(a)]
        else:
            usage()
            assert False, "unhandled option"


    ### Find all *.c and *.h files
    for root, subdirs, files in os.walk(walk_dir):
        list_file_path = os.path.join(root, 'my-directory-list.txt')
        with open(list_file_path, 'wb') as list_file:
            for filename in files:
                file_path = os.path.join(root, filename)
                # if filename.endswith(".asm"):
                #     todo: implement parsing asm comments
                if filename.endswith(".c"):
                    parseHeader(file_path, verbose) # parse .c file
                elif filename.endswith(".h"):
                    parseHeader(file_path, verbose) # parse .h file


    ws2 = wb.add_sheet('Headers overview')
    ws2.col(1).width = 40000;
    ws2.write(0,0,"Header number",style3)
    ws2.write(0,1,"Comment",style3)
    linecounter = 1;
    for header in listOfHeaders:
        if verbose:
            print("\n\n-----------------------------------------Header " + str(listOfHeaders.index(header)) + "---------------------------------------------------------")
            print(header)
        ws2.write(linecounter,0,listOfHeaders.index(header),style3)
        ws2.write(linecounter,1,header,style2)
        linecounter += 1
    if len(listOfHeaders) == 0:
        print("ERROR: No comments found, exiting!")
    else:
        wb.save(output)
        if verbose:
            print("\n\n-----------------------------------------------------------------------------------------------------------")
        print(str(len(listOfHeaders)) + " headers found, protocol saved to " + output + ".")

if __name__ == "__main__":
    main()
